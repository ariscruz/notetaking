This project requires the ff:

* working postgres database locally, with postgres schema set
* java 8 jdk

Import the project via Intellij

Let it download it's dependencies

Once ready, hit Run "NoteTakingApplication". 

Once done, go to your browser and enter the this url: http://localhost:8080

Use the ff users to login into the application

* username1/password
* username2/password

