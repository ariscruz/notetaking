package com.rs2.candidate.assesstment.application.notetaking.repository;

import com.rs2.candidate.assesstment.application.notetaking.entity.Label;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LabelRepository extends JpaRepository<Label, Integer> {

    @Query("SELECT label FROM Label label ORDER BY label.id ASC")
    List<Label> findAllSorted();

    @Query("SELECT l FROM Label l WHERE l.name = :labelName")
    Label findByName(@Param("labelName") final String labelName);
}
