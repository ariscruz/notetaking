package com.rs2.candidate.assesstment.application.notetaking.security;

import com.rs2.candidate.assesstment.application.notetaking.entity.User;
import com.rs2.candidate.assesstment.application.notetaking.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    public CustomUserDetailsService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final User user = userRepository.findByUsername(username);
        if (user != null) {
            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), generateRoleAuthorities());
        } else {
            throw new UsernameNotFoundException("Invalid username or password");
        }
    }

    private Collection<? extends GrantedAuthority> generateRoleAuthorities() {
        Collection<SimpleGrantedAuthority> rolesToAuthority = new java.util.ArrayList<>();
        rolesToAuthority.add(new SimpleGrantedAuthority("User"));
        return rolesToAuthority;
    }
}
