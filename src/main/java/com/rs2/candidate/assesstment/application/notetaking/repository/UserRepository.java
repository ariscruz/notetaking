package com.rs2.candidate.assesstment.application.notetaking.repository;

import com.rs2.candidate.assesstment.application.notetaking.entity.User;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUsername(final String username);
}
