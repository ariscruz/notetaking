package com.rs2.candidate.assesstment.application.notetaking.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "note")
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    @Column(length = 32, nullable = false)
    @Size(min = 1, max = 32, message = "{Size.Note.Title}")
    @NotEmpty(message = "{NotEmpty.Note.Title}")
    private String title;

    @Column(length = 300, nullable = false)
    @Size(min = 1, max = 300, message = "{Size.Note.Content}")
    @NotEmpty(message = "{NotEmpty.Note.Content}")
    private String content;

    @OneToMany(mappedBy = "note", cascade = CascadeType.ALL)
    private Set<NoteLabel> noteLabel;

    @Transient
    private String label;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<NoteLabel> getNoteLabel() {
        return noteLabel;
    }

    public void setNoteLabel(Set<NoteLabel> noteLabel) {
        this.noteLabel = noteLabel;
    }

    public String getLabel() {
        Set<NoteLabel> currentNoteLabel = getNoteLabel();
        if (currentNoteLabel == null) {
            return label;
        }
        label = currentNoteLabel.stream()
                .map(nl -> nl.getLabel().getName())
                .collect(Collectors.joining(","));

        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
