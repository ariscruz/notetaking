package com.rs2.candidate.assesstment.application.notetaking.service.impl;

import com.rs2.candidate.assesstment.application.notetaking.entity.NoteLabel;
import com.rs2.candidate.assesstment.application.notetaking.repository.NoteLabelRepository;
import com.rs2.candidate.assesstment.application.notetaking.service.NoteLabelService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteLabelServiceImpl implements NoteLabelService {

    private NoteLabelRepository noteLabelRepository;

    public NoteLabelServiceImpl(final NoteLabelRepository noteLabelRepository) {
        this.noteLabelRepository = noteLabelRepository;
    }
    @Override
    public NoteLabel saveNoteLabel(final NoteLabel noteLabel) {
        return noteLabelRepository.save(noteLabel);
    }

    @Override
    public NoteLabel findByNoteAndLabel(final Integer noteId, final Integer labelId) {
        return noteLabelRepository.findByNoteAndLabel(noteId, labelId);
    }

    @Override
    public List<NoteLabel> findNoteLabelsByLabelId(final Integer labelId) {
        return noteLabelRepository.findNoteLabelsByLabelId(labelId);
    }

}
