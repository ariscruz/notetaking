package com.rs2.candidate.assesstment.application.notetaking.service;

import com.rs2.candidate.assesstment.application.notetaking.entity.NoteLabel;

import java.util.List;

public interface NoteLabelService {
    NoteLabel saveNoteLabel(final NoteLabel noteLabel);
    NoteLabel findByNoteAndLabel(final Integer noteId, final Integer labelId);
    List<NoteLabel> findNoteLabelsByLabelId(final Integer labelId);
}
