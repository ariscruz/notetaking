package com.rs2.candidate.assesstment.application.notetaking.service;

import com.rs2.candidate.assesstment.application.notetaking.entity.Label;

import java.util.List;

public interface LabelService {
    Label saveLabel(final Label label);
    Label findByName(final String labelName);
    List<Label> listAll();
    void deleteById(final Integer labelId);
}
