package com.rs2.candidate.assesstment.application.notetaking.controller;

import com.rs2.candidate.assesstment.application.notetaking.entity.Label;
import com.rs2.candidate.assesstment.application.notetaking.entity.Note;
import com.rs2.candidate.assesstment.application.notetaking.entity.NoteLabel;
import com.rs2.candidate.assesstment.application.notetaking.service.LabelService;
import com.rs2.candidate.assesstment.application.notetaking.service.NoteLabelService;
import com.rs2.candidate.assesstment.application.notetaking.service.NoteTakingService;
import org.springframework.core.NestedExceptionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Controller
public class NoteTakingController {

    private NoteTakingService noteTakingService;
    private LabelService labelService;
    private NoteLabelService noteLabelService;

    public NoteTakingController(final NoteTakingService noteTakingService,
                                final LabelService labelService,
                                final NoteLabelService noteLabelService) {
        this.noteTakingService = noteTakingService;
        this.labelService = labelService;
        this.noteLabelService = noteLabelService;
    }

    @GetMapping("/notes")
    public String listAllNotes(final Model model) {
        final List<Note> noteList = new ArrayList<>(noteTakingService.listAllNotes());

        final List<String> labelList = labelService.listAll()
                                        .stream()
                                        .map(Label::getName)
                                        .collect(Collectors.toList());

        model.addAttribute("noteList", noteList);
        model.addAttribute("labelList", labelList);
        return "notes";
    }

    @GetMapping("/notes/new")
    public String showAddNoteForm(final Note note) {
        return "note-add-form";
    }

    @PostMapping("/notes/save")
    public String createNote(@Valid final Note note,
                             final BindingResult bindingResult,
                             final Model model) {
        if (bindingResult.hasErrors()) {
            return "note-add-form";
        }

        final String noteLabel = note.getLabel() != null ? note.getLabel() : "";

        List<Label> savedLabels;
        try {
            savedLabels = persistLabel(noteLabel);
        } catch (ConstraintViolationException cve) {
            final List<String> violations = new ArrayList<>();
            cve.getConstraintViolations().forEach(constraintViolation ->
                    violations.add(
                            String.format("%s", constraintViolation.getMessage())
                    ));

            model.addAttribute("message", String.join("", violations));
            return "note-add-form";
        }

        final Note savedNote = noteTakingService.saveNote(note);
        // persist notelabels
        if (!savedLabels.isEmpty()) {
            for (final Label label: savedLabels) {
                persistNoteLabel(label, savedNote);
            }
        }

        return "redirect:/notes";
    }

    @GetMapping("/notes/edit/{id}")
    public String showEditForm(@PathVariable("id") final Integer noteId,
                               final Model model) {
        final Note note = noteTakingService.findById(noteId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid note id:" + noteId));

        model.addAttribute("note", note);
        return "note-update-form";
    }

    @PutMapping("/notes/update/{id}")
    public String updateNote(@PathVariable("id") final Integer noteId,
                             @Valid final Note note,
                             final BindingResult bindingResult,
                             final Model model) {
        if (bindingResult.hasErrors()) {
            note.setId(noteId);
            return "note-update-form";
        }

        final String noteLabel = note.getLabel() != null ? note.getLabel() : "";

        List<Label> savedLabels;
        try {
            savedLabels = persistLabel(noteLabel);
        } catch (ConstraintViolationException cve) {
            final List<String> violations = new ArrayList<>();
            cve.getConstraintViolations().forEach(constraintViolation ->
                    violations.add(
                            String.format("%s", constraintViolation.getMessage())
                    ));

            model.addAttribute("message", String.join("", violations));
            return "note-update-form";
        }

        final Note updatedNote = noteTakingService.updateNote(note);
        // persist notelabels
        if (!savedLabels.isEmpty()) {
            for (final Label label: savedLabels) {
                persistNoteLabel(label, updatedNote);
            }
        }

        return "redirect:/notes";
    }

    @GetMapping("/notes/delete/{id}")
    public String deleteNote(@PathVariable("id") final Integer noteId) {

        final Note note = noteTakingService.findById(noteId).get();

        for (NoteLabel noteLabel : note.getNoteLabel()) {
            final Label currentLabel = noteLabel.getLabel();
            // checks if the labels are only associated with this note
            final List<NoteLabel> noteLabelsForThisLabel = noteLabelService.findNoteLabelsByLabelId(currentLabel.getId());
            if (noteLabelsForThisLabel.size() == 1) {
                // safe to delete
                labelService.deleteById(currentLabel.getId());
            }
        }

        note.setNoteLabel(null);
        noteTakingService.deleteNoteById(note.getId());

        return "redirect:/notes";
    }

    @GetMapping("/labelAutoComplete")
    @ResponseBody
    public List<String> listAllLabels() {
        final List<String> labelList = labelService.listAll()
                                        .stream()
                                        .map(Label::getName)
                                        .collect(Collectors.toList());
        return labelList;
    }

    private List<Label> persistLabel(final String noteLabel) throws ConstraintViolationException {

        final List<Label> returnedLabels = new ArrayList<>();
        // split the labels
        final List<String> splittedLabels = asList(noteLabel.split(",", -1));
        for (String s: splittedLabels) {
            if (s.trim().isEmpty()) {
                // skip empty string
                continue;
            }
            final Label existingLabel = labelService.findByName(s);
            if (existingLabel == null) {
                try {
                    final Label newLabel = new Label();
                    newLabel.setName(s);

                    final Label savedLabel = labelService.saveLabel(newLabel);
                    returnedLabels.add(savedLabel);
                } catch(Exception e) {
                    final Throwable cause = NestedExceptionUtils.getRootCause(e);
                    if (cause instanceof ConstraintViolationException) {
                        throw (ConstraintViolationException) cause;
                    }
                }
            } else {
                returnedLabels.add(existingLabel);
            }
        }
        return returnedLabels;
    }

    private void persistNoteLabel(final Label label, final Note note) {

        final NoteLabel existingNoteLabel = noteLabelService.findByNoteAndLabel(note.getId(), label.getId());
        if (existingNoteLabel == null) {
            final NoteLabel noteLabel1 = new NoteLabel();
            noteLabel1.setNote(note);
            noteLabel1.setLabel(label);
            noteLabelService.saveNoteLabel(noteLabel1);
        }
    }
}
