package com.rs2.candidate.assesstment.application.notetaking.service.impl;

import com.rs2.candidate.assesstment.application.notetaking.entity.Note;
import com.rs2.candidate.assesstment.application.notetaking.repository.NoteTakingRepository;
import com.rs2.candidate.assesstment.application.notetaking.service.NoteTakingService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NoteTakingServiceImpl implements NoteTakingService {

    private NoteTakingRepository noteTakingRepository;

    public NoteTakingServiceImpl(final NoteTakingRepository noteTakingRepository) {
        this.noteTakingRepository = noteTakingRepository;
    }

    @Override
    public List<Note> listAllNotes() {
        return noteTakingRepository.findAllSorted();
    }

    @Override
    public Optional<Note> findById(final Integer noteId) {
        return noteTakingRepository.findById(noteId);
    }

    @Override
    public Note updateNote(final Note note) {
        return noteTakingRepository.save(note);
    }

    @Override
    public Note saveNote(final Note note) {
        return noteTakingRepository.save(note);
    }

    @Override
    public void deleteNoteById(final Integer noteId) {
        noteTakingRepository.deleteById(noteId);
    }
}
