package com.rs2.candidate.assesstment.application.notetaking.repository;

import com.rs2.candidate.assesstment.application.notetaking.entity.NoteLabel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteLabelRepository extends JpaRepository<NoteLabel, Integer> {

    @Query("SELECT nl FROM NoteLabel nl WHERE nl.note.id = :noteId AND nl.label.id = :labelId")
    NoteLabel findByNoteAndLabel(@Param("noteId") final Integer noteId, @Param("labelId") final Integer labelId);

    @Query("SELECT nl FROM NoteLabel nl WHERE nl.label.id = :labelId")
    List<NoteLabel> findNoteLabelsByLabelId(@Param("labelId") final Integer labelId);
}
