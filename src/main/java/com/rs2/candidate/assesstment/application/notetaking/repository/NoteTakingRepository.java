package com.rs2.candidate.assesstment.application.notetaking.repository;

import com.rs2.candidate.assesstment.application.notetaking.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteTakingRepository extends JpaRepository<Note, Integer> {

    @Query("SELECT note FROM Note note ORDER BY note.id ASC")
    List<Note> findAllSorted();
}
