package com.rs2.candidate.assesstment.application.notetaking.service.impl;

import com.rs2.candidate.assesstment.application.notetaking.entity.Label;
import com.rs2.candidate.assesstment.application.notetaking.repository.LabelRepository;
import com.rs2.candidate.assesstment.application.notetaking.service.LabelService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LabelServiceImpl implements LabelService {

    private LabelRepository labelRepository;

    public LabelServiceImpl(final LabelRepository labelRepository) {
        this.labelRepository = labelRepository;
    }

    @Override
    public Label saveLabel(final Label label) {
        return labelRepository.save(label);
    }

    @Override
    public Label findByName(final String labelName) {
        return labelRepository.findByName(labelName);
    }

    @Override
    public List<Label> listAll() {
        return labelRepository.findAllSorted();
    }

    @Override
    public void deleteById(final Integer labelId) {
        labelRepository.deleteById(labelId);
    }
}
