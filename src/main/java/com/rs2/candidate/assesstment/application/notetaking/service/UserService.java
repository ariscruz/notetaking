package com.rs2.candidate.assesstment.application.notetaking.service;

import com.rs2.candidate.assesstment.application.notetaking.entity.User;

public interface UserService {
    User findUserByUsername(final String username);
    User createUser(final User user);
}
