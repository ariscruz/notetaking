package com.rs2.candidate.assesstment.application.notetaking.service.impl;

import com.rs2.candidate.assesstment.application.notetaking.entity.User;
import com.rs2.candidate.assesstment.application.notetaking.repository.UserRepository;
import com.rs2.candidate.assesstment.application.notetaking.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService  {

    private UserRepository userRepository;

    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findUserByUsername(final String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User createUser(final User user) {
        return userRepository.save(user);
    }
}
