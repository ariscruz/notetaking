package com.rs2.candidate.assesstment.application.notetaking.service;

import com.rs2.candidate.assesstment.application.notetaking.entity.Note;
import java.util.List;
import java.util.Optional;

public interface NoteTakingService {
    List<Note> listAllNotes();
    Optional<Note> findById(final Integer noteId);
    Note updateNote(final Note note);
    Note saveNote(final Note note);
    void deleteNoteById(final Integer noteId);
}
