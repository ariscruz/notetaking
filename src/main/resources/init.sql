CREATE TABLE IF NOT EXISTS "user"
(
   id INTEGER PRIMARY KEY NOT NULL,
   name VARCHAR(32) NOT NULL,
   surname VARCHAR(32) NOT NULL,
   username VARCHAR(32) NOT NULL,
   password VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS note
(
    id INTEGER PRIMARY KEY NOT NULL,
    title VARCHAR(32) NOT NULL,
    content VARCHAR(300) NOT NULL
);

CREATE TABLE IF NOT EXISTS label
(
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR(32) NOT NULL
);

CREATE TABLE IF NOT EXISTS note_label
(
    id INTEGER NOT NULL,
    noteId INTEGER NOT NULL,
    labelId INTEGER NOT NULL,
    CONSTRAINT pk_noteId_labelId PRIMARY KEY (noteId, labelId),
    CONSTRAINT fk_note_id FOREIGN KEY (noteId) REFERENCES note(id) ON DELETE CASCADE,
    CONSTRAINT fk_label_id FOREIGN KEY (labelId) REFERENCES label(id) ON DELETE CASCADE
);

TRUNCATE TABLE "user";
-- decrypted password is 'password'
INSERT INTO "user"(id, name, surname, username, password) values(1, 'user1', 'surname1', 'username1', '$2a$10$zK/2Lj0lCTcr1PJpn3n2t.eyfaYk3PPFqbCSQhJv1ensO/1.TWche');
INSERT INTO "user"(id, name, surname, username, password) values(2, 'user2', 'surname2', 'username2', '$2a$10$zK/2Lj0lCTcr1PJpn3n2t.eyfaYk3PPFqbCSQhJv1ensO/1.TWche');