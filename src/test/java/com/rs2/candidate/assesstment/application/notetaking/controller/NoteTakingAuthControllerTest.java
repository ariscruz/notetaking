package com.rs2.candidate.assesstment.application.notetaking.controller;

import com.rs2.candidate.assesstment.application.notetaking.security.NoteTakingWebSecurity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@Import(NoteTakingWebSecurity.class)
@WebMvcTest(NoteTakingAuthController.class)
public class NoteTakingAuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldAllowAccessForAnonymousUser() throws Exception {
        this.mockMvc
                .perform(get("/index"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));

        this.mockMvc
                .perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }
}
