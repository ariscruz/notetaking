package com.rs2.candidate.assesstment.application.notetaking.controller;

import com.rs2.candidate.assesstment.application.notetaking.entity.Label;
import com.rs2.candidate.assesstment.application.notetaking.entity.Note;
import com.rs2.candidate.assesstment.application.notetaking.entity.NoteLabel;
import com.rs2.candidate.assesstment.application.notetaking.security.NoteTakingWebSecurity;
import com.rs2.candidate.assesstment.application.notetaking.service.LabelService;
import com.rs2.candidate.assesstment.application.notetaking.service.NoteLabelService;
import com.rs2.candidate.assesstment.application.notetaking.service.NoteTakingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@Import(NoteTakingWebSecurity.class)
@WebMvcTest(NoteTakingController.class)
public class NoteTakingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean(name = "noteTakingService")
    private NoteTakingService mockNoteTakingService;

    @MockBean(name = "labelService")
    private LabelService mockLabelService;

    @MockBean(name = "noteLabelService")
    private NoteLabelService mockNoteLabelService;

    @Test
    @WithMockUser(username = "user1")
    void shouldAllowAccessForAuthenticatedUser() throws Exception {
        this.mockMvc
                .perform(get("/notes"))
                .andExpect(status().isOk())
                .andExpect(view().name("notes"));
    }


    @Test
    @WithMockUser(username = "user1")
    void noteShouldBeCreated() throws Exception {

        int noteId = 1, labelid = 1;
        final Label savedLabel = new Label();
        savedLabel.setId(labelid);
        savedLabel.setName("label1");

        final Note savedNote = new Note();
        savedNote.setId(noteId);
        savedNote.setTitle("note1 title");
        savedNote.setContent("note1 content");

        when(mockLabelService.saveLabel(any())).thenReturn(savedLabel);
        when(mockNoteTakingService.saveNote(any())).thenReturn(savedNote);
        when(mockNoteLabelService.findByNoteAndLabel(savedNote.getId(), savedLabel.getId())).thenReturn(null);

        this.mockMvc
                .perform(post("/notes/save")
                        .param("title", "note1 title")
                        .param("content", "note1 content")
                        .param("label", "label1")
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/notes"));
    }

    @Test
    @WithMockUser(username = "user1")
    void noteShouldBeNotBeCreatedWhenTitleIsNotPassed() throws Exception {

        this.mockMvc
                .perform(post("/notes/save")
                        .param("title", "")
                        .param("content", "note1 content")
                        .param("label", "label1")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser(username = "user1")
    void noteShouldBeNotBeCreatedWhenTitleIsMoreThan32Characters() throws Exception {

        this.mockMvc
                .perform(post("/notes/save")
                        .param("title", "Lorem ipsum dolor sit amet, consectetur")
                        .param("content", "note1 content")
                        .param("label", "")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser(username = "user1")
    void noteShouldBeDeletedEvenIfLabelIsLinkedToOtherNote() throws Exception {

        int noteId = 1, labelid = 1;
        final Label savedLabel = new Label();
        savedLabel.setId(labelid);
        savedLabel.setName("label1");

        final Note savedNote1 = new Note();
        savedNote1.setId(noteId);
        savedNote1.setTitle("note1 title");
        savedNote1.setContent("note1 content");

        final Note savedNote2 = new Note();
        savedNote2.setId(2);
        savedNote2.setTitle("note2 title");
        savedNote2.setContent("note2 content");

        List<NoteLabel> noteLabels = new ArrayList<>();

        final NoteLabel noteLabel1 = new NoteLabel();
        noteLabel1.setNote(savedNote1);
        noteLabel1.setLabel(savedLabel);

        final NoteLabel noteLabel2 = new NoteLabel();
        noteLabel2.setNote(savedNote2);
        noteLabel2.setLabel(savedLabel);

        noteLabels.add(noteLabel1);
        noteLabels.add(noteLabel2);

        Set<NoteLabel> noteLabelsForThisNote = new HashSet<>();
        noteLabelsForThisNote.add(noteLabel1);

        savedNote1.setNoteLabel(noteLabelsForThisNote);

        when(mockLabelService.saveLabel(any())).thenReturn(savedLabel);
        when(mockNoteTakingService.findById(1)).thenReturn(Optional.of(savedNote1));
        when(mockNoteLabelService.findNoteLabelsByLabelId(labelid)).thenReturn(noteLabels);

        this.mockMvc
                .perform(get("/notes/delete/1")
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/notes"));
    }

}
